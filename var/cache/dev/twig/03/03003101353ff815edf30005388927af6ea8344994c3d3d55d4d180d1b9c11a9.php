<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* maleteo.html.twig */
class __TwigTemplate_54516666145e084af0c1998023917638115e2eb018d5a0c3a6468ea79f70251a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "maleteo.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Maleteo</title>
    <link rel=\"stylesheet\" href=\"assets/styles/_reset.scss\">
    <link rel=\"stylesheet\" href=\"assets/styles/style.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran&display=swap\" rel=\"stylesheet\">
    <script src=\"jsmaleteo.js\"></script>
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>
    <script src=\"jsmaleteosqrl.js\"></script>
</head>
<body>
    <div class=\"wrapper\">
        <div class=\"container-fluid\">
    <header>
        <img class=\"logo\" src=\"assets/images/logo.svg\" alt=\"logo\">
    </header>

    <main>
        <div class=\"row\" >
            <div class=\"titulo col-md-12\">
                <!--<img class=\"titulo__foto\" src=\"images/jumbo.jpg\" alt=\"maletas\">-->
                <div class=\"titulo__cabeza col-md-6\">
                    <div class=\"titulo__interiorcabeza \">
                        <h1>Gana dinero guardando equipaje a viajeros como tú</h1>
                        <div class=\"titulo__iconos\">    
                            <a class=\"icono1\" href=\"#\"><img src=\"assets/images/app-store.svg\" alt=\"appstore\"></a>
                            <a class=\"icono2\" href=\"#\"><img src=\"assets/images/google-play.svg\" alt=\"googleplay\"></a>
                        </div>    
                    </div>    
                </div>
            </div> 
        </div>

        <div class=\"funciona\">
            <h2 class=\"funciona__titulo\">¿Cómo funciona?</h2>
            <ol class=\"row\">
                <li class=\"col-md-4 col-xs-12\">
                    <div class=\"funciona__bolita\">
                        <p class=\"funciona__numero\">1</p>
                    </div>
                    <div class=\"funciona__cosa col-md-12 col-xs-10\">
                        <h3>Date de alta</h3>
                        <p>Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados.</p>
                    </div>    
                </li>
                <li class=\"col-md-4 col-xs-12\">
                    <div class=\"funciona__bolita\">
                        <p class=\"funciona__numero\">2</p>
                    </div>
                    <div class=\"funciona__cosa col-md-12 col-xs-10\">
                        <h3>Publica tus espacios, horarios y tarifas</h3>
                        <p>En un lugar de La Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo.</p>
                    </div>    
                </li>
                <li class=\"col-md-4 col-xs-12\">
                    <div class=\"funciona__bolita\">
                        <p class=\"funciona__numero\">3</p>
                    </div>
                    <div class=\"funciona__cosa col-md-12 col-xs-10\">
                        <h3>Recibe viajeros y gana dinero</h3>
                        <p>No ha mucho tiempo que vivía un hidalgo de los de lanza en astillero.</p>
                    </div>    
                </li>
            </ol>
        </div>

        <div class=\"formulario row\">
            <div class=\"formulario__movil col-md-6\">
                <img class=\"col-md-6 col-xs-12\" src=\"assets/images/iPhoneX.png\" alt=\"iPhoneX\">
            </div>
            <div id=\"bloque0\" class=\"col-md-5\">  
                <div id=\"bloque1\" class=\"formulario_contenido\">
                    
                    ";
        // line 78
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 78, $this->source); })()), 'form_start', ["attr" => ["id" => "formulario", "class" => "formulario__form"]]);
        echo "
                    <p class=\"formulario__demo\">Solicita una demo</p>
                    <div class=\"formulario__input\">
                        ";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 81, $this->source); })()), "Nombre", [], "any", false, false, false, 81), 'label');
        echo "
                        ";
        // line 82
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 82, $this->source); })()), "Nombre", [], "any", false, false, false, 82), 'widget', ["attr" => ["class" => "formulario__inputcampo", "placeholder" => "Nombre"]]);
        echo "
                    </div>
                    <div class=\"formulario__input\">
                        ";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 85, $this->source); })()), "Email", [], "any", false, false, false, 85), 'label');
        echo "
                        ";
        // line 86
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 86, $this->source); })()), "Email", [], "any", false, false, false, 86), 'widget', ["attr" => ["class" => "formulario__inputcampo", "placeholder" => "Email"]]);
        echo "
                    </div>
                    <div class=\"formulario__input\">
                        ";
        // line 89
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 89, $this->source); })()), "Horario", [], "any", false, false, false, 89), 'label');
        echo "
                        ";
        // line 90
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 90, $this->source); })()), "Horario", [], "any", false, false, false, 90), 'widget', ["attr" => ["class" => "formulario__inputcampo", "placeholder" => "Horario"]]);
        echo "
                    </div>
                    <div class=\"formulario__input\">
                        ";
        // line 93
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 93, $this->source); })()), "Ciudad", [], "any", false, false, false, 93), 'label');
        echo "
                        ";
        // line 94
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 94, $this->source); })()), "Ciudad", [], "any", false, false, false, 94), 'widget', ["attr" => ["class" => "formulario__city"]]);
        echo "
                    </div>

                    <div class=\"formulario__check\">
                        ";
        // line 98
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 98, $this->source); })()), "Validacion", [], "any", false, false, false, 98), 'widget', ["attr" => ["class" => "formulario__checkbox"]]);
        echo "
                        ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 99, $this->source); })()), "Validacion", [], "any", false, false, false, 99), 'label');
        echo "
                    </div>
               
                    <div class=\"formulario__enviar\">
                        ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 103, $this->source); })()), "Enviar", [], "any", false, false, false, 103), 'row', ["attr" => ["class" => "formulario__enviar"]]);
        echo "
                    </div>
                
                    ";
        // line 106
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["DemoForm"]) || array_key_exists("DemoForm", $context) ? $context["DemoForm"] : (function () { throw new RuntimeError('Variable "DemoForm" does not exist.', 106, $this->source); })()), 'form_end');
        echo "
                    <div class=\"h3\">
                    ";
        // line 108
        echo twig_escape_filter($this->env, (((isset($context["mensaje"]) || array_key_exists("mensaje", $context))) ? (_twig_default_filter((isset($context["mensaje"]) || array_key_exists("mensaje", $context) ? $context["mensaje"] : (function () { throw new RuntimeError('Variable "mensaje" does not exist.', 108, $this->source); })()))) : ("")), "html", null, true);
        echo " 
                    </div>
                </div>

           <!--     <form class=\"formulario__form\" action=\"#\" method=\"POST\">
                    <p class=\"formulario__demo\">Solicita una demo</p>
                        <div class=\"formulario__input\">
                            <label for=\"Nombre\">Nombre</label>
                            <input type=\"text\" name=\"nombre\" placeholder=\"Nombre\">
                        </div>    
                        <div class=\"formulario__input\">
                            <label for=\"Email\">Email</label>
                            <input type=\"email\" name=\"mail\" placeholder=\"email\">
                        </div>
                        <div class=\"formulario__input\">    
                            <label for=\"horario\">Horario preferido</label>
                            <input type=\"datetime\" name=\"horario\" placeholder=\"9:00-12:00\">
                        </div>    
                        <div class=\"formulario__input\">    
                            <label for=\"ciudad\">Ciudad</label>
                            <select class=\"formulario__city\">
                                <option value=\"Madrid\">Madrid</option>
                                <option value=\"Barcelona\">Barcelona</option>
                            </select>
                        </div>
                        <label class=\"formulario__check\">    
                            <input class=\"formulario__checkbox\" type=\"checkbox\" name=\"condiciones\">
                             He leído y acepto la política de privacidad
                        </label>
                    <input class=\"formulario__enviar\" type=\"submit\" value=\"Enviar\">
                </form> -->
            </div>
        </div>    
        
        <div class=\"opinion\">
            <h2 class=\"opinion__titulo\">Opiniones de otros guardianes</h2>
            <div class=\"row\"> 
                <div class=\"opinion__bloques col-md-4 col-xs-12\">
                    <p class=\"opinion__opis\">Tras el primer éxito internacional de la pachamama y de la bomba de king áfrica, bailar chimpun olé</p>
                    <!--<div class=\"opinion__triangulo\"></div>
                    <div class=\"opinion__triangulo2\"></div>-->
                    <div class=\"opinion__pie\">
                        <p class=\"opinion__autor\">Sergio Garnacho</p>
                        <p class=\"opinion__lugar\">Tetuán, Madrid</p>
                    </div>
                </div>
                
                <div class=\"opinion__bloques col-md-4 col-xs-12\">
                    <p class=\"opinion__opis\">Yo estoy dispuesto a levantarme y a abandonar la mesa porque yo he venido aquí a hablar de mi libro</p>
                    <!--<div class=\"opinion__triangulo\"></div>
                    <div class=\"opinion__triangulo2\"></div>-->
                    <div class=\"opinion__pie opinion__bloques2\">
                        <p class=\"opinion__autor\">Francismo Umbral</p>
                        <p class=\"opinion__lugar\">Tetuán, Madrid</p>
                    </div>
                </div>
                <div class=\"opinion__bloques col-md-4 col-xs-12\">
                    <p class=\"opinion__opis\">Es el vecino el que elige el alcalde y es el alcalde el que quiere que sean los vecinos el alcalde</p>
                    <!--<div class=\"opinion__triangulo\"></div>
                    <div class=\"opinion__triangulo2\"></div>-->
                    <div class=\"opinion__pie\">
                        <p class=\"opinion__autor\">Mariano Rajoy</p>
                        <p class=\"opinion__lugar\">Moncloa, Madrid</p>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"row\">
            <h2 class=\"mapa__titulo\">Guardianes cerca de ti</h2>
            <div class=\"mapa col-md-12 col-xs-12\">
                <div id=\"listado\" class=\"listado col-md-5 col-xs-12\">
                   
                    
                </div>
                <div class=\"col-md-7 col-xs-12\">
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3035.7478653748626!2d-3.6968836851849094!3d40.458717161009794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229775edcab1b%3A0xbd4c6a7181e44101!2sUpgrade%20Hub%20-%20Cursos%20TIC!5e0!3m2!1ses!2ses!4v1576971166668!5m2!1ses!2ses\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                    <!--<div class=\"mapa__puntero col-md-1\">
                        <img src=\"images/map-marker.svg\" alt=\"puntero\">
                    </div>
                    <div class=\"mapa__puntero2 col-md-5\">
                        <img src=\"images/map-marker.svg\" alt=\"puntero\">
                    </div>
                    <div class=\"mapa__puntero3 col-md-6\">
                        <img src=\"images/map-marker.svg\" alt=\"puntero\">
                    </div>-->
                </div>    
            </div>
        </div>

    <!--    <div class=\"mapa\">
            <h2 class=\"mapa__titulo\">Guardianes cerca de ti</h2>
            <div class=\"mapa__pincho row\">
                <iframe class=\"col-md-12\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3610.106581338165!2d-3.695568674640565!3d40.45920806770073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229775edcab1b%3A0xbd4c6a7181e44101!2sUpgrade%20Hub%20-%20Cursos%20TIC!5e0!3m2!1ses!2ses!4v1574991450478!5m2!1ses!2ses\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>

                <!--<div class=\"mapa__puntero col-md-1\">
                    <img src=\"images/map-marker.svg\" alt=\"puntero\">
                </div>
                <div class=\"mapa__puntero2 col-md-5\">
                    <img src=\"images/map-marker.svg\" alt=\"puntero\">
                </div>
                <div class=\"mapa__puntero3 col-md-6\">
                    <img src=\"images/map-marker.svg\" alt=\"puntero\">
                </div>-
            </div>    
        </div>-->
    </main>
    </div>
</div>
<footer class=\"pie\">
    <div class=\"pie__logo\">
        <img src=\"assets/images/logoblanco.svg\" alt=\"logoblanco\">
    </div>
    <p class=\"pie__pie\">Hecho con &#128147 en Madrid</p>
</footer>
</body>
</html>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "maleteo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 108,  183 => 106,  177 => 103,  170 => 99,  166 => 98,  159 => 94,  155 => 93,  149 => 90,  145 => 89,  139 => 86,  135 => 85,  129 => 82,  125 => 81,  119 => 78,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Maleteo</title>
    <link rel=\"stylesheet\" href=\"assets/styles/_reset.scss\">
    <link rel=\"stylesheet\" href=\"assets/styles/style.css\">
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran&display=swap\" rel=\"stylesheet\">
    <script src=\"jsmaleteo.js\"></script>
    <script src=\"https://cdn.jsdelivr.net/npm/squirrelly@7.5.0/dist/squirrelly.min.js\"></script>
    <script src=\"jsmaleteosqrl.js\"></script>
</head>
<body>
    <div class=\"wrapper\">
        <div class=\"container-fluid\">
    <header>
        <img class=\"logo\" src=\"assets/images/logo.svg\" alt=\"logo\">
    </header>

    <main>
        <div class=\"row\" >
            <div class=\"titulo col-md-12\">
                <!--<img class=\"titulo__foto\" src=\"images/jumbo.jpg\" alt=\"maletas\">-->
                <div class=\"titulo__cabeza col-md-6\">
                    <div class=\"titulo__interiorcabeza \">
                        <h1>Gana dinero guardando equipaje a viajeros como tú</h1>
                        <div class=\"titulo__iconos\">    
                            <a class=\"icono1\" href=\"#\"><img src=\"assets/images/app-store.svg\" alt=\"appstore\"></a>
                            <a class=\"icono2\" href=\"#\"><img src=\"assets/images/google-play.svg\" alt=\"googleplay\"></a>
                        </div>    
                    </div>    
                </div>
            </div> 
        </div>

        <div class=\"funciona\">
            <h2 class=\"funciona__titulo\">¿Cómo funciona?</h2>
            <ol class=\"row\">
                <li class=\"col-md-4 col-xs-12\">
                    <div class=\"funciona__bolita\">
                        <p class=\"funciona__numero\">1</p>
                    </div>
                    <div class=\"funciona__cosa col-md-12 col-xs-10\">
                        <h3>Date de alta</h3>
                        <p>Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados.</p>
                    </div>    
                </li>
                <li class=\"col-md-4 col-xs-12\">
                    <div class=\"funciona__bolita\">
                        <p class=\"funciona__numero\">2</p>
                    </div>
                    <div class=\"funciona__cosa col-md-12 col-xs-10\">
                        <h3>Publica tus espacios, horarios y tarifas</h3>
                        <p>En un lugar de La Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo.</p>
                    </div>    
                </li>
                <li class=\"col-md-4 col-xs-12\">
                    <div class=\"funciona__bolita\">
                        <p class=\"funciona__numero\">3</p>
                    </div>
                    <div class=\"funciona__cosa col-md-12 col-xs-10\">
                        <h3>Recibe viajeros y gana dinero</h3>
                        <p>No ha mucho tiempo que vivía un hidalgo de los de lanza en astillero.</p>
                    </div>    
                </li>
            </ol>
        </div>

        <div class=\"formulario row\">
            <div class=\"formulario__movil col-md-6\">
                <img class=\"col-md-6 col-xs-12\" src=\"assets/images/iPhoneX.png\" alt=\"iPhoneX\">
            </div>
            <div id=\"bloque0\" class=\"col-md-5\">  
                <div id=\"bloque1\" class=\"formulario_contenido\">
                    
                    {{ form_start(DemoForm, {attr:{id: \"formulario\", class:\"formulario__form\"}}) }}
                    <p class=\"formulario__demo\">Solicita una demo</p>
                    <div class=\"formulario__input\">
                        {{ form_label(DemoForm.Nombre)}}
                        {{ form_widget(DemoForm.Nombre, {attr:{class: 'formulario__inputcampo', placeholder:'Nombre'}})}}
                    </div>
                    <div class=\"formulario__input\">
                        {{ form_label(DemoForm.Email)}}
                        {{ form_widget(DemoForm.Email, {attr:{class: 'formulario__inputcampo', placeholder:'Email'}})}}
                    </div>
                    <div class=\"formulario__input\">
                        {{ form_label(DemoForm.Horario)}}
                        {{ form_widget(DemoForm.Horario, {attr:{class: 'formulario__inputcampo', placeholder:'Horario'}})}}
                    </div>
                    <div class=\"formulario__input\">
                        {{ form_label(DemoForm.Ciudad)}}
                        {{ form_widget(DemoForm.Ciudad, {attr:{class: 'formulario__city'}})}}
                    </div>

                    <div class=\"formulario__check\">
                        {{ form_widget(DemoForm.Validacion, {attr:{class: 'formulario__checkbox'}}) }}
                        {{ form_label(DemoForm.Validacion) }}
                    </div>
               
                    <div class=\"formulario__enviar\">
                        {{ form_row(DemoForm.Enviar, {attr:{class: \"formulario__enviar\"}}) }}
                    </div>
                
                    {{ form_end(DemoForm) }}
                    <div class=\"h3\">
                    {{ mensaje|default}} 
                    </div>
                </div>

           <!--     <form class=\"formulario__form\" action=\"#\" method=\"POST\">
                    <p class=\"formulario__demo\">Solicita una demo</p>
                        <div class=\"formulario__input\">
                            <label for=\"Nombre\">Nombre</label>
                            <input type=\"text\" name=\"nombre\" placeholder=\"Nombre\">
                        </div>    
                        <div class=\"formulario__input\">
                            <label for=\"Email\">Email</label>
                            <input type=\"email\" name=\"mail\" placeholder=\"email\">
                        </div>
                        <div class=\"formulario__input\">    
                            <label for=\"horario\">Horario preferido</label>
                            <input type=\"datetime\" name=\"horario\" placeholder=\"9:00-12:00\">
                        </div>    
                        <div class=\"formulario__input\">    
                            <label for=\"ciudad\">Ciudad</label>
                            <select class=\"formulario__city\">
                                <option value=\"Madrid\">Madrid</option>
                                <option value=\"Barcelona\">Barcelona</option>
                            </select>
                        </div>
                        <label class=\"formulario__check\">    
                            <input class=\"formulario__checkbox\" type=\"checkbox\" name=\"condiciones\">
                             He leído y acepto la política de privacidad
                        </label>
                    <input class=\"formulario__enviar\" type=\"submit\" value=\"Enviar\">
                </form> -->
            </div>
        </div>    
        
        <div class=\"opinion\">
            <h2 class=\"opinion__titulo\">Opiniones de otros guardianes</h2>
            <div class=\"row\"> 
                <div class=\"opinion__bloques col-md-4 col-xs-12\">
                    <p class=\"opinion__opis\">Tras el primer éxito internacional de la pachamama y de la bomba de king áfrica, bailar chimpun olé</p>
                    <!--<div class=\"opinion__triangulo\"></div>
                    <div class=\"opinion__triangulo2\"></div>-->
                    <div class=\"opinion__pie\">
                        <p class=\"opinion__autor\">Sergio Garnacho</p>
                        <p class=\"opinion__lugar\">Tetuán, Madrid</p>
                    </div>
                </div>
                
                <div class=\"opinion__bloques col-md-4 col-xs-12\">
                    <p class=\"opinion__opis\">Yo estoy dispuesto a levantarme y a abandonar la mesa porque yo he venido aquí a hablar de mi libro</p>
                    <!--<div class=\"opinion__triangulo\"></div>
                    <div class=\"opinion__triangulo2\"></div>-->
                    <div class=\"opinion__pie opinion__bloques2\">
                        <p class=\"opinion__autor\">Francismo Umbral</p>
                        <p class=\"opinion__lugar\">Tetuán, Madrid</p>
                    </div>
                </div>
                <div class=\"opinion__bloques col-md-4 col-xs-12\">
                    <p class=\"opinion__opis\">Es el vecino el que elige el alcalde y es el alcalde el que quiere que sean los vecinos el alcalde</p>
                    <!--<div class=\"opinion__triangulo\"></div>
                    <div class=\"opinion__triangulo2\"></div>-->
                    <div class=\"opinion__pie\">
                        <p class=\"opinion__autor\">Mariano Rajoy</p>
                        <p class=\"opinion__lugar\">Moncloa, Madrid</p>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"row\">
            <h2 class=\"mapa__titulo\">Guardianes cerca de ti</h2>
            <div class=\"mapa col-md-12 col-xs-12\">
                <div id=\"listado\" class=\"listado col-md-5 col-xs-12\">
                   
                    
                </div>
                <div class=\"col-md-7 col-xs-12\">
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3035.7478653748626!2d-3.6968836851849094!3d40.458717161009794!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229775edcab1b%3A0xbd4c6a7181e44101!2sUpgrade%20Hub%20-%20Cursos%20TIC!5e0!3m2!1ses!2ses!4v1576971166668!5m2!1ses!2ses\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
                    <!--<div class=\"mapa__puntero col-md-1\">
                        <img src=\"images/map-marker.svg\" alt=\"puntero\">
                    </div>
                    <div class=\"mapa__puntero2 col-md-5\">
                        <img src=\"images/map-marker.svg\" alt=\"puntero\">
                    </div>
                    <div class=\"mapa__puntero3 col-md-6\">
                        <img src=\"images/map-marker.svg\" alt=\"puntero\">
                    </div>-->
                </div>    
            </div>
        </div>

    <!--    <div class=\"mapa\">
            <h2 class=\"mapa__titulo\">Guardianes cerca de ti</h2>
            <div class=\"mapa__pincho row\">
                <iframe class=\"col-md-12\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3610.106581338165!2d-3.695568674640565!3d40.45920806770073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4229775edcab1b%3A0xbd4c6a7181e44101!2sUpgrade%20Hub%20-%20Cursos%20TIC!5e0!3m2!1ses!2ses!4v1574991450478!5m2!1ses!2ses\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>

                <!--<div class=\"mapa__puntero col-md-1\">
                    <img src=\"images/map-marker.svg\" alt=\"puntero\">
                </div>
                <div class=\"mapa__puntero2 col-md-5\">
                    <img src=\"images/map-marker.svg\" alt=\"puntero\">
                </div>
                <div class=\"mapa__puntero3 col-md-6\">
                    <img src=\"images/map-marker.svg\" alt=\"puntero\">
                </div>-
            </div>    
        </div>-->
    </main>
    </div>
</div>
<footer class=\"pie\">
    <div class=\"pie__logo\">
        <img src=\"assets/images/logoblanco.svg\" alt=\"logoblanco\">
    </div>
    <p class=\"pie__pie\">Hecho con &#128147 en Madrid</p>
</footer>
</body>
</html>", "maleteo.html.twig", "/application/templates/maleteo.html.twig");
    }
}
