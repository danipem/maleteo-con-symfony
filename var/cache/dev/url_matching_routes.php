<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/peliculas' => [[['_route' => 'app_default_peliculas', '_controller' => 'App\\Controller\\DefaultController::peliculas'], null, null, null, false, false, null]],
        '/maleteo' => [[['_route' => 'inicio', '_controller' => 'App\\Controller\\MaleteoController::maleteo'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\DefaultController::index'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/calcular/([^/]++)(*:60)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        60 => [
            [['_route' => 'app_default_calcular', '_controller' => 'App\\Controller\\DefaultController::calcular'], ['numero'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
