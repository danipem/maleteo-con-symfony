const submitHandler = (event) => {
    event.preventDefault();
    const formdata = new FormData(event.target)
    fetch('/maleteo', {
        method: 'post',
        body: formdata
    }).then((res) => {
    console.log(res); //si en lugar de console.log pongo >>> 
    //res.json().then((constantenueva)=>{console.log(constantenueva)}) se convierte a JSON
    
    const contenedorform = document.getElementById('bloque1');
    const cont1 = document.getElementById('bloque0');
    
    
    const contrespuesta = document.createElement('div');
    contrespuesta.id = 'contrespuesta';
    
    const respuesta = document.createElement('h3');
    respuesta.id = 'respuesta'
    respuesta.classList.add('formulario__enviado')
    respuesta.innerText = '¡Muchas gracias! \n Su petición se ha realizado correctamente'
    
    contenedorform.style = 'display: none';
    contenedorform.remove();
    
    contrespuesta.appendChild(respuesta);
    cont1.appendChild(contrespuesta);
    })
    
    
   }

document.addEventListener("DOMContentLoaded", function() {
    var form = document.getElementById("formulario");

    form.addEventListener("submit", submitHandler);
})