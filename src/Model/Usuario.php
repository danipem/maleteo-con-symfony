<?php

namespace App\Model;

class Usuario{

    public $registrado;
    public $nombre;

    public function __construct($nombre, $registrado)
    {
        $this->nombre = $nombre;
        $this->registrado = $registrado;
    }

    public function getRegitrado(){
        return $this->registrado;
    }

}