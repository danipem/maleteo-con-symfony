<?php

namespace App\Form;

use App\Entity\Maleteo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DemoFormType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        //definimos los campos del formulario
        $builder->add('Nombre');
        $builder->add('Email', EmailType::class);
        $builder->add('Horario');
        $builder->add('Ciudad', ChoiceType::class,
    [
            'choices' => [
                'Madrid' => 'Madrid',
                'Tres Cantos' => 'Tres Cantos',
                'Barcelona' => 'Barcelona',
                'Guarromán' => 'Guarroman',
                'Shangai' => 'Shangai'
            ]
    ]);
        $builder->add ('Validacion', CheckboxType::class,
            [
                'required' => true,
                'mapped' => false,
                'label' => 'He leído y acepto la política de privacidad '
                
            ]);
        $builder->add ('Enviar', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
        {
        $resolver->setDefaults([
        "data_class" => Maleteo::class
        ]);
        }

}