<?php

namespace App\Controller;

use App\Form\DemoFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Usuario;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class MaleteoController extends AbstractController{

    /**
     * @Route("/contact/{nombre}", methods={"POST","GET"})
     */
 /*   public function contact($nombre){

        $usuario  = new Usuario($nombre,false);
        return $this->render("index.html.twig",["usuario"=>$usuario]);

    }
*/
    /**
     * @Route("/maleteo", name="inicio")
     */
    public function maleteo(EntityManagerInterface $emi, Request $request){
        
        $DemoForm = $this->createForm(DemoFormType::class);
        $DemoForm->handleRequest(($request));

        if($DemoForm->isSubmitted() && $DemoForm->isValid())
        {
            $visitantes = $DemoForm->getData();
            $emi->persist($visitantes);
            $emi->flush();

            return $this->render("maleteo.html.twig",
            [
                'DemoForm' => $DemoForm->createView(),
                'mensaje' => '¡Enviado con éxito!'
            ]);
        }

        return $this->render("maleteo.html.twig",
        [
            'DemoForm' => $DemoForm->createView()
        ]);

    }

}

//composer require doctrine
//composer require maker --dev
//./bin/console make:entity >>> para crear la clase con los campos de la bbdd
//make:migration >>> para conectar con la base de datos