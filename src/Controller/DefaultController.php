<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

//Todos los controladores deben devolver un objeto de tipo Response
class DefaultController{
    /**
     * @Route("/calcular/{numero}")
     */
    public function calcular($numero){
        
        if(is_numeric($numero)){

        if($numero%2 == 0){

            return new Response("El número $numero es par");
        
        }else {
            return new Response("El número $numero es impar");
        }
        }else{
            $cadena = "";
            $palabra = str_split($numero);
            for($i = 0; $i< count($palabra); $i++){
                if($i%2 == 0){
                    $cadena .= strtoupper($palabra[$i]);
                }else{
                    $cadena .= strtolower($palabra[$i]);
                }
            }
            return new Response("Tu cadena modificada tete $cadena");
        }


    }


    public function index(){

        return new Response("Este es mi primer controlador");
    }

    public function saludos(){

        return new Response("<h1>Esto es un saludo </h1>");
    }
    /**
     * @Route("/peliculas")
     */
    public function peliculas(){

        return new Response("Listado de pelicuas");
    }
    
}